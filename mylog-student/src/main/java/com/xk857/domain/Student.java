package com.xk857.domain;

import lombok.Data;

import javax.persistence.*;
import java.util.Date;

@Data
@Entity
@Table(name="student")
public class Student {

  @Id
  @Column(name = "sid")
  @GeneratedValue(strategy=GenerationType.IDENTITY)
  private int sid;

  @Column(name = "realname")
  private String realname;

  @Column(name = "gender")
  private String gender;

  @Column(name = "profession")
  private String profession;//专业

  @Column(name = "birthday")
  private Date birthday;//生日

  @Column(name = "create_time")
  private Date createTime;//创建日期

  @Column(name = "polity")
  private String polity;//政治面貌

  @Column(name = "stay")
  private String stay;//住宿否

  @Column(name = "pic")
  private String pic;//照片

}
