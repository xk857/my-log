package com.xk857.controller;


import com.xk857.annotation.Log;
import com.xk857.domain.Student;
import com.xk857.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * 学生信息控制层
 * @author CV大魔王
 * @date 2020-12-03
 */
@RestController
@RequestMapping("/student")
public class StudentController {

    @Autowired
    private StudentService studentService;


    @Log("查询所有学生")
    @GetMapping
    public List<Student> findAll(){
        return studentService.queryAll();
    }

    /**
     * 自己仿造异常
     * @return
     */
    @Log("根据id查询")
    @GetMapping("/test")
    public String findById(){
        int a = 1/0;
        return "模拟数据";
    }
}
