package com.xk857.service.impl;

import com.xk857.domain.Student;
import com.xk857.repository.StudentRepository;
import com.xk857.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;


/**
 * Student服务接口实现类
 * @author CV大魔王
 * @date 2020-12-03
 */
@Service
public class StudentServiceImpl implements StudentService {

    @Autowired
    private StudentRepository studentRepository;


    @Override
    public List<Student> queryAll() {
        return studentRepository.findAll();
    }
}
