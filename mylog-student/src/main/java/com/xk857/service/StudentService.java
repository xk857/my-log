package com.xk857.service;

import com.xk857.domain.Student;

import java.util.List;

/**
 * Student服务实现
 * @author CV大魔王
 * @date 2020-12-03
 */
public interface StudentService {


    /**
     * 分页查询
     * @return 学生集合
     */
    List<Student> queryAll();
}
