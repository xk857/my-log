package com.xk857.annotation;


import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author CV大魔王
 * @date 2020-12-03
 */
@Target(ElementType.METHOD) //声明该注解仅适用与方法
@Retention(RetentionPolicy.RUNTIME) //注解不仅被保存到class文件中，jvm加载class文件之后，仍然存在；
public @interface Log {
    String value() default "";
}
