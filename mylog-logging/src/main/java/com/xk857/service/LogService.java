package com.xk857.service;


import com.xk857.domain.Log;
import org.aspectj.lang.ProceedingJoinPoint;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;

import java.util.List;

/**
 * @author CV大魔王
 * @date 2020-11-24
 */
public interface LogService {


    /**
     * 查询全部数据
     * @return /
     */
    List<Log> queryAll();


    /**
     * 保存日志数据
     * @param joinPoint /
     * @param log 日志实体
     */

    void save(ProceedingJoinPoint joinPoint, Log log);

    /**
     * 查询异常详情
     * @param id 日志ID
     * @return Object
     */
    Object findByErrDetail(Long id);
}
